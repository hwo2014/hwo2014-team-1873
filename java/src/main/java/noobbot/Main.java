package noobbot;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.*;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Main {

    static GameInitData gameInitData;
    static Track track;
    static String botName;

    static Car ourCar;
    static int ourCarIndex;

    static double halfDistanceBetweenLanes;

    static boolean turboAvailable = false;
    Turbo turbo;

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        //new Main(reader, writer, new JoinRace(botName, botKey, "usa", "watWhyWhere", 2)); // ONLY FOR TESTING
        new Main(reader, writer, new Join(botName, botKey));    // ONLY FOR CI
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
        //PrintWriter fileWriter = new PrintWriter(botName + "Log.txt", "UTF-8");
        // uncomment for logging

        this.writer = writer;
        String line = null;
        //fileWriter.println("Sending Message: " + join.toJson());
        send(join);

        boolean firstCarPosition = true; // flag for the very first (special) carPositionMessage
        // ---- MAIN GAME LOOP  ----
        while ((line = reader.readLine()) != null) {

            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            System.out.println("Got message: " + line);
            // uncomment for logging

            String dataInJSON = gson.toJson(msgFromServer.data);
            if (msgFromServer.msgType.equals("carPositions")) {

                // This is what we get usually during the race; this is the place where the intelligence goes :)
                CarPositionsMessage carPositionsMessage = gson.fromJson(line, CarPositionsMessage.class);
                if (firstCarPosition) {
                    for (Car someCar : carPositionsMessage.listOfCars) {
                        if (someCar.id.name.equals(botName)) {
                            ourCar = someCar;
                            break;
                        }
                    }
                    ourCarIndex = carPositionsMessage.listOfCars.indexOf(ourCar);
                    firstCarPosition = false;
                } else {
                    PiecePosition oldPiecePosition = ourCar.piecePosition; // save for distance calculation
                    ourCar = carPositionsMessage.listOfCars.get(ourCarIndex); // update our car (otherwise we are stuck with old info)
                    double distanceTravelled = calculateDistance(oldPiecePosition, ourCar.piecePosition);
                    ourCar.speed = distanceTravelled * 60; // each tick is 1/60th of a second
                    SendMsg s = decideWhatToDo(carPositionsMessage);
                    //fileWriter.println("Sending Message: " + s.toJson());
                    //  uncomment for logging
                    send(s);
                }

            } else if (msgFromServer.msgType.equals("join") | msgFromServer.msgType.equals("joinRace")) {
                // First message
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                // Contains the race track! Important for planning
                gameInitData = gson.fromJson(dataInJSON, GameInitData.class);
                track = gameInitData.race.track;
                writeDefaultPenalties();
                determineSwitchDirections();
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                // End of Race (some alternatives (finish?))
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                // Start of Race
                System.out.println("Race start");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("spawn")) {
                // track.pieces.get(ourCarIndex).crashPenalty += 0.1;
                // track.pieces.get(ourCarIndex - 1).crashPenalty += 0.05;
                send(new Ping());
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
                turboAvailable = true;
                turbo = gson.fromJson(line, Turbo.class);
            } else {
                send(new Ping());
            }
        }
        //fileWriter.close();
        // uncomment for logging
    }

    private SendMsg decideWhatToDo(CarPositionsMessage carPositionsMessage) {
        SendMsg s;
        int currentPieceIndex = ourCar.piecePosition.pieceIndex;

        //System.out.println(ourCar.speed);

        if (ourCar.speed < 30) {
            return new Throttle(1.0);
        }

        // THROTTLE OPTIMIZATION
        double throttle = 0.92;
        s = new Throttle(throttle);
        // END THROTTLE OPTIMIZATION

        // TURBO USAGE
        if (turboAvailable) {
            boolean straight = true;
            ListIterator<Piece> pieceIterator = track.pieces.listIterator(currentPieceIndex + 1);
            int i = 1;
            while (pieceIterator.hasNext() && i <= 5) {
                straight = straight && (Math.abs(pieceIterator.next().angle) == 0);
                i++;
            }
            if (straight) {
                // if the next 5 pieces are straight; lez go!
                s = new TurboMsg("BURNIN' BYTES");
                turboAvailable = false;
            }
        }
        // END TURBO USAGE

        // LANE SWITCHING
        ListIterator<Piece> pieceListIterator = track.pieces.listIterator(currentPieceIndex + 1); // Iterator pointing AFTER current piece, next is next piece
        double currentPieceLength = track.pieces.get(currentPieceIndex).length;
        if (pieceListIterator.hasNext() && pieceListIterator.next().laneChangePossible
                && ourCar.piecePosition.inPieceDistance < (currentPieceLength - (currentPieceLength - 10))
                && track.pieces.get(pieceListIterator.previousIndex()).switchDirection != null) {
            // if we're close to the next switch and it isn't the last one
            // TODO: make the offset speed dependant
            String dir = track.pieces.get(pieceListIterator.previousIndex()).switchDirection.getDirectionString();
            if (!dir.equals(""))
                s = new Switch(dir); // Switch in desired direction
        }
        // END LANE SWITCHING

        return s;
    }

    enum Direction {
        RIGHT("Right"),
        LEFT("Left"),
        NONE("");

        private String directionString;

        private Direction(final String directionString) {
            this.directionString = directionString;
        }

        protected String getDirectionString() { return directionString; }
    }

    private double calculateDistance(PiecePosition pos1, PiecePosition pos2) {
        // Calculates the distance between two PiecePositions
        // used in speed calculation
        Piece piece1 = track.pieces.get(pos1.pieceIndex);
        Piece piece2 = track.pieces.get(pos2.pieceIndex);

        // CURVES
        if (piece1.length == 0) {
            piece1.length = (piece1.radius - 2 * halfDistanceBetweenLanes) * Math.toRadians(Math.abs(piece1.angle));
        } else if (piece2.length == 0 ) {
            piece2.length = (piece2.radius - 2 * halfDistanceBetweenLanes ) * Math.toRadians(Math.abs(piece2.angle));
        }
        // SWITCHES
        if (pos1.inPieceDistance > piece1.length) {
            piece1.length = Math.sqrt(Math.pow(2 * halfDistanceBetweenLanes, 2) + Math.pow(piece1.length, 2));
        } else if (pos2.inPieceDistance > piece2.length) {
            piece2.length = Math.sqrt(Math.pow(2 * halfDistanceBetweenLanes, 2) + Math.pow(piece2.length, 2));
        }
        if (pos1.pieceIndex == pos2.pieceIndex) {
            return Math.abs(pos2.inPieceDistance - pos1.inPieceDistance);
        } else if (pos1.pieceIndex == 0 && pos2.pieceIndex > 1) {
            return (track.pieces.getLast().length - pos2.inPieceDistance) + pos1.inPieceDistance;
        } else if (pos2.pieceIndex == 0 && pos1.pieceIndex > 1) {
            return (track.pieces.getLast().length - pos1.inPieceDistance) + pos2.inPieceDistance;
        } else if (pos1.pieceIndex < pos2.pieceIndex) {
            return (piece1.length - pos1.inPieceDistance) + pos2.inPieceDistance;
        } else {
            return (piece2.length - pos2.inPieceDistance) + pos1.inPieceDistance;
        }
    }


    private void determineSwitchDirections() {
        // determine distance between lanes
        if (track.lanes.size() > 1) {
            halfDistanceBetweenLanes = Math.abs(track.lanes.get(0).distanceFromCenter - track.lanes.get(1).distanceFromCenter) / 2.0;
        }

        ArrayList<Piece> switches = new ArrayList<>();
        ListIterator<Piece> pieceListIterator = track.pieces.listIterator(0);
        while (pieceListIterator.hasNext()) {
            if (pieceListIterator.next().laneChangePossible) {
                switches.add(track.pieces.get(pieceListIterator.previousIndex())); // first switch in the track
            }
        }
        pieceListIterator = switches.listIterator(); // pieceListIterator now iterates over only switches

        if (switches.size() > 1) { // only if we have more than one switch
            Piece thisSwitch = pieceListIterator.next();
            Piece nextSwitch = pieceListIterator.next();
            thisSwitch.switchDirection = optimalPath(track.pieces.indexOf(thisSwitch), track.pieces.indexOf(nextSwitch));

            while (pieceListIterator.hasNext()) {
                // write switchDirection to switch, so that we can decide in which direction we should switch
                thisSwitch = nextSwitch;
                nextSwitch = pieceListIterator.next();
                thisSwitch.switchDirection = optimalPath(track.pieces.indexOf(thisSwitch), track.pieces.indexOf(nextSwitch));
            }
        }
    }

    private void writeDefaultPenalties() {
        for (Piece somePiece : track.pieces) {
            if (somePiece.angle != 0) {

            }
        }
    }

    private Direction optimalPath(int switch1Index, int switch2Index) {
        int lTotal = 0, rTotal = 0;


        ListIterator<Piece> pieceIterator = track.pieces.listIterator(switch1Index + 1); // Iterator points after switch one, next is the next piece
        while (pieceIterator.nextIndex() < switch2Index) {
            Piece current_piece = pieceIterator.next();
            if (pieceIterator.next().angle < 0) { // Left Turn
                lTotal += (current_piece.radius - halfDistanceBetweenLanes) * Math.toRadians(Math.abs(current_piece.angle));
                rTotal += (current_piece.radius + halfDistanceBetweenLanes) * Math.toRadians(Math.abs(current_piece.angle));
            } else if (pieceIterator.next().angle > 0) { // Right Turn
                lTotal += (current_piece.radius + halfDistanceBetweenLanes) * Math.toRadians(Math.abs(current_piece.angle));
                rTotal += (current_piece.radius - halfDistanceBetweenLanes) * Math.toRadians(Math.abs(current_piece.angle));
            } else { // Straight piece
                lTotal += current_piece.length;
                rTotal += current_piece.length;
            }
        }

        if (lTotal < rTotal) {
            return Direction.LEFT;
        } else if (lTotal > rTotal) {
            return Direction.RIGHT;
        } else {
            return Direction.NONE;
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

}

// class for sending Messages to the server (e.g join; but also lane switches)
abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

// MsgWrapper
// Used for distinguishing between MsgTypes -> detailed parsing
// Can be used for incoming and outgoing messages
// -> see second constructor
class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}


class CreateRace extends SendMsg {
    protected final BotID botId;
    protected final String trackName;
    protected final String password;
    protected int carCount;

    CreateRace(String name, String key, String trackName, String password, int carCount) {
        this.botId = new BotID(name, key);
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}

class JoinRace extends SendMsg {
    protected final BotID botId;
    protected final String trackName;
    protected final String password;
    protected int carCount;

    JoinRace(String name, String key, String trackName, String password, int carCount) {
        this.botId = new BotID(name, key);
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class Turbo {
    protected double turboDurationMilliseconds;
    protected int turboDurationTicks;
    protected double turboFactor;
}

class BotID {
    String name;
    String key;

    BotID(String name, String key) {
        this.name = name;
        this.key = key;
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Switch extends SendMsg {
    public final String direction;

    protected Switch(String direction) {
        this.direction = direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }

    @Override
    protected String msgData() {
        return direction;
    }
}

class TurboMsg extends SendMsg {
    public final String turboMessage;

    protected TurboMsg(String turboMessage) {
        this.turboMessage = turboMessage;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }

    @Override
    protected String msgData() {
        return turboMessage;
    }
}

// Can be sent at any time -> signifies no action; just telling the server we're still around
class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

// We send a Throttle object to say how fast we want to go
// I _think_ this works as such that it keeps applying the last Throttle object until a newer
// instance is sent
class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

// Parsing Classes -> This is all the info we get from the server
// Here we implement interesting functions that create meaning from
// the listOfCars.

// You can have a look at listOfCars I logged during two races, one with crashes and one without
// in customRaceLog[NoCrash|WithCrashes].json

// Parses all of a carPositions Message
class CarPositionsMessage {
    //protected String msgType;
    @SerializedName("data")
    protected List<Car> listOfCars;
    protected String gameID;
    protected int gameTick;
}

// Parses all of a gameInit Message
class GameInitData {
    protected Race race;
}

class Race {
    protected Track track;
    protected List<CarInfo> carInfos;
    protected RaceSession raceSession;
}

class Track {
    protected String id;
    protected String name;
    protected LinkedList<Piece> pieces;
    protected List<Lane> lanes;
    protected StartingPoint startingPoint;
}

class CarInfo {
    protected CarID id;
    protected CarDimensions dimensions;
    protected PiecePosition piecePosition;
}

class Car {
    protected CarID id;
    protected double angle;
    PiecePosition piecePosition;

    protected double speed;
}


class RaceSession {
    protected int laps;
    protected int maxLapTime;
    protected boolean quickRace;
}

class CarID {
    protected String name;
    protected String color;
}

class CarDimensions {
    protected double length;
    protected double width;
    protected double guiderFlagPosition;
}

class PiecePosition {
    protected int pieceIndex;
    protected double inPieceDistance;
    protected LaneChange lane;
    protected int lap;
}

// This one is sort of weird -> It contains the end and start index of the lanes
// I don't know whether they're actually ints, maybe they go float during a switch?
class LaneChange {
    protected int startLaneIndex;
    protected int endLaneIndex;
}

class Piece {
    protected double length;
    protected double radius;
    protected double angle;
    // we can't call the boolean switch (as the protocol does), since switch is reserved,
    // so we tell GSON that we want switch in laneChangePossible
    @SerializedName("switch")
    protected boolean laneChangePossible;

    protected Main.Direction switchDirection;
    protected double crashPenalty = 0.0; // increased when we crash in this piece
}

class Lane {
    protected int index;
    protected int distanceFromCenter;
}

class StartingPoint {
    protected Point position;
    protected double angle;
}